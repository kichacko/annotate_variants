#!/bin/sh

#  Script2.sh
#
#
#  Created by Kieran Chacko on 5/8/17.
#
#

# Input Files
# 1: Reference Genome
# 2: Query Genome
# 1; Reference GFF file

# Load Modules
module load mummer/3.23
module load annovar/2014Nov12
module load python
module load py_packages
export PATH="$PATH:/hpc/users/vanbah01/bin/x86_64"

# Create Basenames and folders
reference=`basename "${1}" .fasta`
query=`basename "${2}" .fasta`
output="${4}"
mkdir="${output}"

# Run NucDIFF
python ~/dwns/NucDiff/nucdiff.py "${1}" "${2}" "${output}-NucDIFF" "${output}"
cp ./"${output}-NucDIFF"/results/"${output}_ref_snps.gff" ./
cp ./"${output}-NucDIFF"/results/"${output}_ref_struct.gff" ./

### SNV Analysis ###

# Prepare ANNOVAR input file
sed -i -n '/^#/!p' "${output}_ref_snps.gff"
awk 'BEGIN{FS=OFS="\t"} {gsub("ID.*query_bases=","",$0); gsub(";ref_bases=","\t",$0); gsub(";color=.*","",$0); print $1, $4, $5, $10, $9 }' "${output}_ref_snps.gff" > "${output}_ANNOVAR-SNV"

# Prepare Reference Genome File --> refGene.txt
gff3ToGenePred "${3}" "${reference}.gp"      # .gff --> .gp
awk ' BEGIN{OFS=FS="\t"} {{$(NF+1)=1; print $NF, $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15;}}' "${reference}.gp" > "${reference}_refGene.txt"

# Move Files into db Directory
mkdir "${output}-Annovar"
mv "${reference}_refGene.txt" ./"${output}-Annovar"

# Prepare Genome Sequence File --> .fa
faSplit byname "${1}" ./"${output}-Annovar"/
retrieve_seq_from_fasta.pl -format refGene -seqdir "${output}-Annovar" "${output}-Annovar"/"${reference}_refGene.txt"
cd ./"${output}-Annovar"
mv "${reference}_refGene.txt.fa" "${reference}_refGeneMrna.fa"
cd ..

# Prepare Reference Link File --> refLink.txt
awk ' BEGIN{OFS=FS="\t"} {print $1, $1".gene";}' "${reference}.gp" > "${reference}_refLink.txt"
mv "${reference}_refLink.txt" ./"${output}-Annovar"

# Run ANNOVAR
annotate_variation.pl -buildver "${reference}" "${output}_ANNOVAR-SNV" ./"${output}-Annovar"

# Combine protein names with ANNOVAR output
awk 'BEGIN{FS=OFS="\t"} {sub(/:/,"\t")}1 {print $4, $2, $3, $5, $6, $7, $8, $9}' "${output}_ANNOVAR-SNV.exonic_variant_function" | sed 's/.*:p.//g' | sed 's/.*:c.*,//g' | sed 's/,//g' | awk 'BEGIN{FS=OFS="\t"} {print $3, $2, $4, $5, $6, $7, $8, $9, $1}' > "${output}_ANNOVAR-SNV.exonic_variant_function_refined"
cut -f 9 "${3}"  | sed 's/^ID=//g' | sed 's/;.*product=/\tDescription = /g'  | sed '/^PROKKA.*/!d' > "${reference}_refined.gff3"
/hpc/users/vanbah01/opt/utility/bin/join-by-ids -a 1 -1 1 -2 1 "${output}_ANNOVAR-SNV.exonic_variant_function_refined" "${reference}_refined.gff3" > "${output}_Exonic_SNV-OUTPUT"

### SV Analysis ###

# Prepare ANNOVAR input file
sed -i -n '/^#/!p' "${output}_ref_struct.gff"
awk 'BEGIN{FS=OFS="\t"}
{
    gsub("ID.*;Name=","",$0);
    gsub(";.*len=","\t",$0);
    gsub(";query_.*","",$0);

    if ($9=="insertion" || $9=="duplication")
        {$6="-" ; $7="0"};
    if ($9=="deletion"|| $9=="collapsed_repeat")
        {$6="0" ; $7="-"};
    if ($9=="reshuffling.*" || $9=="inversion.*")
        {$6="0" ; $7="0"};
    print $1, $4, $5, $6, $7, $9, $10;
}' "${output}_ref_struct.gff" > "${output}_ANNOVAR-SV"

# Prepare Reference Genome File
sed -n '/^>/q;p' "${3}" > "${reference}-Clean.gff"
mv "${reference}-Clean.gff" ./"${output}-Annovar"

# Run ANNOVAR
annotate_variation.pl -buildver "${reference}" -regionanno -dbtype gff3 --gff3dbfile "${reference}-Clean.gff" "${output}_ANNOVAR-SV" ./"${output}-Annovar" -out "${output}_SV-OUTPUT"

# Combine protein names with ANNOVAR output
awk 'BEGIN{FS=OFS="\t"} {print $2, $8, $3, $4, $5, $6, $7, $9}' "${output}_SV-OUTPUT.${reference}_gff3" | sed 's/Name=//g' > "${output}_SV-OUTPUT_refined"
/hpc/users/vanbah01/opt/utility/bin/join-by-ids -a 1 -1 1 -2 1 "${output}_SV-OUTPUT_refined" "${reference}_refined.gff3" > "${output}_Exonic_SV-OUTPUT"

### Clean Up ###
rm "${reference}.gp"
rm "${reference}_refined.gff3"
rm "${output}_ANNOVAR-SNV"
rm "${output}_ANNOVAR-SNV.exonic_variant_function"
rm "${output}_ANNOVAR-SNV.exonic_variant_function_refined"
rm "${output}_SV-OUTPUT_refined"
rm "${output}_SV-OUTPUT.${reference}_gff3"
rm "${output}_ref_snps.gff"
rm "${output}_ref_struct.gff"

mv *.log ./"${output}-Annovar"
mv "${output}_ANNOVAR-SNV.variant_function" "${output}_All_SNV-OUTPUT"
mv "${output}_ANNOVAR-SV" "${output}_All_SV-OUTPUT"
