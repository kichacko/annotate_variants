# Start in "nucmer_post_reorientation_copy"

#!/bin/bash

mkdir ../annovar

x=$( ls -1 | grep "snv_pt" | sed "s/.*pt\([0-9]*\).*/\1/" | sort -n | uniq )
for i in $x
do
    mkdir pt_"$i"_B_annovar
    cp snv_pt"$i"_B_*_ilm_consensus_vs_pt"$i"_?_* ./pt_"$i"_B_annovar
    cp MRSA_pt"$i"_B_*.fasta ./pt_"$i"_B_annovar
    mv pt_"$i"_B_annovar ../annovar
done

# Move to igb_copy

cd ../igb_copy

y=$( ls -1 | grep "MRSA_pt" | sed "s/.*pt\([0-9]*\).*/\1/" | sort -n | uniq )
for j in $y
do
    cd ./MRSA_pt"$j"_B*
    cp MRSA_pt"$j"_B_*.gff3 ../../annovar/pt_"$i"_B_annovar
done

cd ../../annovar

for k in $y
do
    cd ./pt_"$k"_B_annovar
    annotate_variant.sh snv_pt"$k"_B_*_ilm_consensus_vs_pt"$k"_?_* MRSA_pt"$k"_B_*.fasta MRSA_pt"$k"_B_*.gff3
done